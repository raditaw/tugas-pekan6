@extends('layouts.master')

@section('content-header')
<h1> Buat Account Baru! </h1>
<h3> Sign Up Form </h3>
@endsection

@section('main-content')
    <form action="{{ route("dashboard") }}" method="get">
     @csrf
        <label for="First name"> First name: </label> <br> <br>
             <input type="text" name="name_first" required> <br> <br>
        <label for="last name"> Last name: </label> <br> <br>
             <input type="text" name="name last" required> <br> <br>
        <label for="Gender"> Gender: </label> <br> <br>
             <input type="radio" name="gender" value="L" required> Male <br>
             <input type="radio" name="gender" value="P" required> Female <br>
             <input type="radio" name="gender" value="W" required> Other <br> <br> 
        <label for="Nationality"> Nationality </label> <br> <br>
             <select name="negara" required>
                  <option value="indonesia"> Indonesia </option>
                 <option value="malaysia"> Malaysia </option>
                 <option value="inggris"> English </option>
             </select><br> <br>
        <label for="spoken"> Language Spoken: </label> <br> <br>
             <input type="checkbox" name="language"> Bahasa Indonesia <br>
             <input type="checkbox" name="language"> English <br>
             <input type="checkbox" name="language"> Other <br> <br>
        <label for="Bio" required> Bio: </label><br> <br>
             <textarea name="message" cols="30" rows="10"></textarea><br> <br>
             <input type="submit" required>
     </form>
@endsection