<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "IndexController@index")->name("Index");

Route::get("/register", "AuthController@form")->name("form");
Route::get("/dashboard", "IndexController@dashboard")->name("dashboard");

Route::get("/data-tables", "IndexController@datatables")->name('datatables');
